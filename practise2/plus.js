"use strict";

module.exports = function(){
    var amount = 0;
    if(arguments){
        for (var i = 0; i < arguments.length; ++i){
            amount += parseFloat(arguments[i]);
        }
    }
    return amount;
};