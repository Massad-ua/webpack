"use strict";
//4.1 - 4.2
document.getElementById('loginButton').onclick = function() {

    // ======== Способ 1 (require.ensure) ========== //
    require.ensure(['./login'], function(require) {
        let login = require('./login');
        login();
    });

    // ======== Способ 2 (AMD) ========== //

    //require(['./login'], function(login){
    //    login();
    //});


};


document.getElementById('logoutButton').onclick = function() {

    // ======== Способ 1 (require.ensure) ==
    require.ensure([], function(require) {
        let logout = require('./logout');

        logout();
    });

};


