'use strict';

const webpack = require('webpack');
//4.1 - 4.3
//module.exports = {
//    context: './frontend',
//    entry: './app',
//
//    output: {
//        path: __dirname + '/public',
//        publicPath: '/',
//        filename:   "app.js"
//    },
//
//    resolve:{
//        root: __dirname + "/vendor"
//    },
//
//    module: {
//
//        loaders: [{
//            test: /\.js$/,
//            include: __dirname + '/frontend',
//            loader: 'babel'
//        }],
//
//        noParse:  wrapRegexp(/\/node_modules\/(angular\/angular)/, 'noParse')
//
//    }
//
//
//
//};
//
//function wrapRegexp(regexp, label) {
//    regexp.test = function(path) {
//        console.log(label, path);
//        return RegExp.prototype.test.call(this, path);
//    };
//    return regexp;
//}
module.exports = {
    context: __dirname + '/frontend',
    entry: './home',

    output: {
        path: __dirname + '/public',
        filename:   "home.js"
    },

    module:{
        loaders:[{
            test: /old.js$/,
            loader: "imports?workSettings=>{delay:500}!exports?Work"
        }]
    },

    resolve:{
        root: __dirname + "/vendor",
        alias: {
            old: 'old/dist/old'
        }

    }


};


