'use strict';
const webpack = require('webpack');
const path = require('path');

module.exports = {

    context: path.resolve(__dirname, "frontend"),
    entry:   './main',
    output:  {
        path: path.resolve(__dirname, "public"),
        publicPath: '/',
        filename: '[name].js'
    },


    resolve:{
        modulesDirectories:['node_modules'],
        extensions: ['', '.js']
    },
    resolveLoader:{
        modulesDirectories:['node_modules'],
        moduleTemplates: ['*-loader', '*'],
        extensions: ['','.js']
    },

    module: {

        loaders: [{
            test:   /\.js$/,
            include: path.resolve(__dirname, "frontend"),
            loader: "babel?presets[]=es2015"
        }, {
            test:   /\.jade$/,
            include: path.resolve(__dirname, "frontend"),
            loader: "jade"
        }, {
            test:   /\.css$/,
            include: path.resolve(__dirname, "frontend"),
            // .../node_modules/css-loader/index.js!.../node_modules/autoprefixer-loader/index.js?browsers=last 2 versions!.../frontend/menu/menu.css
            loader: 'style!css!autoprefixer?browsers=last 2 versions'
        }, {
            test:   /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
            include: path.resolve(__dirname, "frontend"),
            loader: 'url?file?name=[path][name].[ext]'
        }]

    }

};


//'use strict';
//
//module.exports = {
//    context: __dirname + '/frontend',
//    entry:   './main',
//    output:  {
//        path:     __dirname + '/public',
//        publicPath: '/',
//        filename: '[name].js'
//    },
//
//    module: {
//
//        loaders: [{
//            test:   /\.js$/,
//            loader: "babel?presets[]=es2015"
//        }, {
//            test:   /\.jade$/,
//            loader: "jade"
//        }, {
//            test:   /\.css$/,
//            // .../node_modules/css-loader/index.js!.../node_modules/autoprefixer-loader/index.js?browsers=last 2 versions!.../frontend/menu/menu.css
//            loader: 'style!css!autoprefixer?browsers=last 2 versions?resolve url'
//        }, {
//            test:   /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
//            loader: 'file?name=[path][name].[ext]'
//        }]
//
//    }
//
//};
//
//


